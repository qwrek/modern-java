package com.binary_studio.tree_max_depth;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		List<Department> departments;

		int depth = 0;

		if (Objects.isNull(rootDepartment)) {
			return depth;
		}
		else {
			depth++;
		}

		departments = rootDepartment.subDepartments.parallelStream().filter(Objects::nonNull)
				.collect(Collectors.toList());

		while (!departments.isEmpty()) {
			depth++;

			departments = departments.stream().flatMap(department -> department.subDepartments.stream())
					.collect(Collectors.toList()).stream().filter(Objects::nonNull).collect(Collectors.toList());
		}

		return depth;
	}

}
