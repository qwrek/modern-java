package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger impactReduction;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public PositiveInteger getImpactReduction() {
		return this.impactReduction;
	}

	public PositiveInteger getShieldRegen() {
		return this.shieldRegen;
	}

	public PositiveInteger getHullRegen() {
		return this.hullRegen;
	}

	public PositiveInteger getCapacitorUsage() {
		return this.capacitorUsage;
	}

	public PositiveInteger getPgRequirement() {
		return this.pgRequirement;
	}

	private DefenciveSubsystemImpl(String name, PositiveInteger powergridConsumption, PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent, PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) {
		this.name = name;
		this.pgRequirement = powergridConsumption;
		this.capacitorUsage = capacitorConsumption;
		this.impactReduction = impactReductionPercent;
		this.shieldRegen = shieldRegeneration;
		this.hullRegen = hullRegeneration;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption, PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent, PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption, impactReductionPercent,
				shieldRegeneration, hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		var reductionRatio = this.impactReduction.value() >= 95
				? 0.95
				: this.impactReduction.value() / 100.0;
		var damage = (int) Math.ceil(incomingDamage.damage.value() * (1 - reductionRatio));

		return new AttackAction(PositiveInteger.of(damage), incomingDamage.attacker, incomingDamage.target, incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

}
