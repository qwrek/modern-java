package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger baseDamage;

	private PositiveInteger optimalSize;

	private PositiveInteger optimalSpeed;

	private PositiveInteger capacitorUsage;

	private PositiveInteger pgRequirement;

	public PositiveInteger getBaseDamage() {
		return this.baseDamage;
	}

	public PositiveInteger getOptimalSize() {
		return this.optimalSize;
	}

	public PositiveInteger getOptimalSpeed() {
		return this.optimalSpeed;
	}

	public PositiveInteger getCapacitorUsage() {
		return this.capacitorUsage;
	}

	public PositiveInteger getPgRequirement() {
		return this.pgRequirement;
	}

	private AttackSubsystemImpl(String name, PositiveInteger powergridRequirments, PositiveInteger capacitorConsumption,
								PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {
		this.name = name;
		this.pgRequirement = powergridRequirments;
		this.baseDamage = baseDamage;
		this.capacitorUsage = capacitorConsumption;
		this.optimalSize = optimalSize;
		this.optimalSpeed = optimalSpeed;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {
		var sizeReductionModifier = (target.getSize().value() >= this.optimalSize.value()) ? 1
				: target.getSize().value() * 1.0 / this.optimalSize.value();
		var speedReductionModifier = (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) ? 1
				: this.optimalSpeed.value() / 2.0 / target.getCurrentSpeed().value();

		return PositiveInteger
				.of((int) Math.ceil(this.baseDamage.value() * Math.min(sizeReductionModifier, speedReductionModifier)));
	}

	@Override
	public String getName() {
		return this.name;
	}

}
