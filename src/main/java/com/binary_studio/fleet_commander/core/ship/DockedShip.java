package com.binary_studio.fleet_commander.core.ship;

import java.util.Objects;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public String getName() {
		return this.name;
	}

	public PositiveInteger getShieldHP() {
		return this.shieldHP;
	}

	public PositiveInteger getHullHP() {
		return this.hullHP;
	}

	public PositiveInteger getCapacitor() {
		return this.capacitor;
	}

	public void setCapacitor(PositiveInteger capacitor) {
		this.capacitor = capacitor;
	}

	public PositiveInteger getCapacitorRegeneration() {
		return this.capacitorRegeneration;
	}

	public PositiveInteger getPg() {
		return this.pg;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	public PositiveInteger getSize() {
		return this.size;
	}

	public AttackSubsystem getAttackSubsystem() {
		return this.attackSubsystem;
	}

	public DefenciveSubsystem getDefenciveSubsystem() {
		return this.defenciveSubsystem;
	}

	public void setShieldHP(PositiveInteger shieldHP) {
		this.shieldHP = shieldHP;
	}

	public void setHullHP(PositiveInteger hullHP) {
		this.hullHP = hullHP;
	}

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed, PositiveInteger size) {

		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.pg = powergridOutput;
		this.capacitor = capacitorAmount;
		this.capacitorRegeneration = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		if (name == null || name.isBlank()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount,
				capacitorRechargeRate, speed, size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {

		if (subsystem == null) {
			this.attackSubsystem = null;
		}

		int pg = Objects.nonNull(subsystem)
				? subsystem.getPowerGridConsumption().value()
				: 0;

		pg += Objects.nonNull(this.defenciveSubsystem)
				? this.defenciveSubsystem.getPowerGridConsumption().value()
				: 0;

		if (pg > this.pg.value()) {
			throw new InsufficientPowergridException(pg - this.pg.value());
		}
		this.attackSubsystem = subsystem;
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {

		if (subsystem == null) {
			this.defenciveSubsystem = null;
		}

		int pg = Objects.nonNull(subsystem)
				? subsystem.getPowerGridConsumption().value()
				: 0;

		pg += Objects.nonNull(this.attackSubsystem)
				? this.attackSubsystem.getPowerGridConsumption().value()
				: 0;

		if (pg > this.pg.value()) {
			throw new InsufficientPowergridException(pg - this.pg.value());
		}
		this.defenciveSubsystem = subsystem;

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem == null && this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}

		return new CombatReadyShip(this);
	}
}
