package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

public final class CombatReadyShip implements CombatReadyVessel {

	DockedShip dockedShip;

	private int maxHullHp;
	private int maxShieldHp;
	private int maxCapacity;
	private boolean isAttacked;

	public CombatReadyShip(DockedShip dockedShip) {
		this.dockedShip = dockedShip;
		this.maxHullHp = dockedShip.getHullHP().value();
		this.maxShieldHp = dockedShip.getShieldHP().value();
		this.maxCapacity = dockedShip.getCapacitor().value();
		this.isAttacked = false;
	}

	@Override
	public void endTurn() {

		this.isAttacked = false;

		var newCap = Math.min((this.dockedShip.getCapacitorRegeneration().value() + this.dockedShip.getCapacitor().value()), this.maxCapacity);
		this.dockedShip.setCapacitor(PositiveInteger.of(newCap));

		var regeneratedPoints = regenerate();

		if (regeneratedPoints.isPresent()) {
			var newShieldHp = this.dockedShip.getShieldHP().value() + regeneratedPoints.get().shieldHPRegenerated.value();
			this.dockedShip.setShieldHP(PositiveInteger.of(newShieldHp));

			var newHullHp = this.dockedShip.getHullHP().value() + regeneratedPoints.get().hullHPRegenerated.value();
			this.dockedShip.setHullHP(PositiveInteger.of(newHullHp));
		}
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.dockedShip.getName();
	}

	@Override
	public PositiveInteger getSize() {
		return this.dockedShip.getSize();
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.dockedShip.getSpeed();
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if ((this.dockedShip.getCapacitor().value() < (this.dockedShip.getAttackSubsystem().getCapacitorUsage().value())) || this.isAttacked) {
			return Optional.empty();
		}

		this.isAttacked = true;

		var newCap = this.dockedShip.getCapacitor().value() - this.dockedShip.getAttackSubsystem().getCapacitorUsage().value();
		this.dockedShip.setCapacitor(PositiveInteger.of(newCap));

		return Optional.of(new AttackAction(
				this.dockedShip.getAttackSubsystem().getBaseDamage(),
				this,
				target,
				this.dockedShip.getAttackSubsystem()));
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		PositiveInteger damage = this.dockedShip.getDefenciveSubsystem().reduceDamage(attack).damage;
		if (this.dockedShip.getHullHP().value() + this.dockedShip.getShieldHP().value() <= damage.value()) {
			return new AttackResult.Destroyed();
		}

		if (this.dockedShip.getShieldHP().value() < damage.value()) {
			PositiveInteger newDamage = PositiveInteger.of(damage.value() - this.dockedShip.getShieldHP().value());
			var newHullHp = this.dockedShip.getHullHP().value() - newDamage.value();
			this.dockedShip.setHullHP(PositiveInteger.of(newHullHp));
			this.dockedShip.setShieldHP(PositiveInteger.of(0));
		}
		else {
			var newShieldHp = this.dockedShip.getShieldHP().value() - damage.value();
			this.dockedShip.setShieldHP(PositiveInteger.of(newShieldHp));
		}

		return new AttackResult.DamageRecived(this.dockedShip.getAttackSubsystem(), damage, attack.target);
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.dockedShip.getCapacitor().value() < (this.dockedShip.getDefenciveSubsystem().getCapacitorUsage().value())) {
			return Optional.empty();
		}

		PositiveInteger shieldRegenerated = this.dockedShip.getDefenciveSubsystem().regenerate().shieldHPRegenerated;
		PositiveInteger hullRegenerated = this.dockedShip.getDefenciveSubsystem().regenerate().hullHPRegenerated;

		var newCap = this.dockedShip.getCapacitor().value() - this.dockedShip.getDefenciveSubsystem().getCapacitorUsage().value();
		this.dockedShip.setCapacitor(PositiveInteger.of(newCap));

		var regeneratedShield = (shieldRegenerated.value() + this.dockedShip.getShieldHP().value()) >= this.maxShieldHp
				? 0
				: shieldRegenerated.value();

		var regeneratedHull = (hullRegenerated.value() + this.dockedShip.getHullHP().value()) >= this.maxHullHp
				? 0
				: hullRegenerated.value();

		return Optional.of(new RegenerateAction(PositiveInteger.of(regeneratedShield), PositiveInteger.of(regeneratedHull)));
	}

}
